object Form2: TForm2
  Left = 0
  Top = 0
  Caption = 'Form2'
  ClientHeight = 302
  ClientWidth = 513
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Menu1: TMemo
    Left = 8
    Top = 8
    Width = 185
    Height = 273
    Lines.Strings = (
      'Menu1')
    TabOrder = 0
  end
  object Bt_tamanho: TButton
    Left = 199
    Top = 24
    Width = 98
    Height = 25
    Caption = 'Tamanho'
    TabOrder = 1
    OnClick = Bt_tamanhoClick
  end
  object Menu2: TMemo
    Left = 309
    Top = 8
    Width = 185
    Height = 273
    Lines.Strings = (
      'Menu2')
    TabOrder = 2
  end
  object Bt_vf: TButton
    Left = 199
    Top = 72
    Width = 98
    Height = 25
    Caption = 'V ou F'
    TabOrder = 3
    OnClick = Bt_vfClick
  end
  object Bt_s_espaco: TButton
    Left = 199
    Top = 119
    Width = 98
    Height = 25
    Caption = 'retirar espa'#231'o'
    TabOrder = 4
    OnClick = Bt_s_espacoClick
  end
  object Bt_minusculo: TButton
    Left = 199
    Top = 160
    Width = 98
    Height = 25
    Caption = 'minusculo'
    TabOrder = 5
    OnClick = Bt_minusculoClick
  end
  object Bt_maiusculo: TButton
    Left = 199
    Top = 208
    Width = 98
    Height = 25
    Caption = 'maiusculo'
    TabOrder = 6
    OnClick = Bt_maiusculoClick
  end
  object bt_replace: TButton
    Left = 199
    Top = 256
    Width = 98
    Height = 25
    Caption = 'Replace'
    TabOrder = 7
    OnClick = bt_replaceClick
  end
end
